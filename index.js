const express = require('express');
const morgan = require('morgan');
const router = require('./route.js');

const port = 8080;
const app = express();

app.use(express.json());
app.use(morgan('combined'));
app.use('/api/files', router);

app.listen(port, () => console.log(`server started http://localhost:${port}`));
