const express = require('express');
const router = express.Router();
const controller = require('./controller');

router.post('/', controller.createFile);
router.get('/', controller.readFiles);
router.get('/:filename', controller.readFile);
router.delete('/:filename', controller.deleteFile);
router.put('/:filename', controller.updateFile);

module.exports = router;
